//
//  Copyright (c) 2015 Estimote, Inc. All rights reserved.
//

#import "ESTAmbientLightDemoVC.h"
@import AVFoundation;

@interface ESTAmbientLightDemoVC () <ESTDeviceConnectableDelegate>

@property (nonatomic) ESTDeviceTypeUtility *beacon;
@property (nonatomic, weak) NSTimer *ambientLightTimer;
@property (nonatomic, strong) NSNumber *flashlightThreshold;

// UI Outlets
@property (weak, nonatomic) IBOutlet UILabel *connectionStatusLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *illuminanceLabel;
@property (weak, nonatomic) IBOutlet UITextField *thresholdTextfield;
@property (weak, nonatomic) IBOutlet UISwitch *darkToSleepSwitch;

@end

#define kRefreshRate 2.0

@implementation ESTAmbientLightDemoVC

- (instancetype)initWithDeviceTypeUtility:(ESTDeviceTypeUtility *)beacon
{
    self = [self init];
    
    if (self)
    {
        if([beacon isKindOfClass:[ESTDeviceTypeUtility class]])
        {
            _beacon = beacon;
        }
        else
        {
            UIAlertView* errorView = [[UIAlertView alloc] initWithTitle:@"Incorrect type of device selected."
                                                                message:@"Ambient light demo works only with Next generation beacons."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            
            [errorView show];
        }
        
        /**
         * This is our threshold value to turn on torch mode
         * for iPhone back camera if ambient light is below this value.
         * There is a high possibility this value will differ for you.
         * You can first run this demo and in console check what values
         * are being printed.
         * Based on this, calibrate threshold to get the expected behaviour.
         */
        _flashlightThreshold = @(35);
    }
    
    return self;
}

#pragma mark - VC Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    /**
    * In order to use sensor data, such as ambient light readings,
    * we need to connect to a beacon first.
    * First step: your class should conform to ESTDeviceConnectableDelegate protocol
    * by implementing its methods.
    * Before calling connect method remember to set up App ID and App Token (AppDelegate.m)
    */
    if(self.beacon)
    {
        [self connect];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.beacon disconnect];
    [self.ambientLightTimer invalidate];
    [self switchTorchModeStateBasedOnIlluminance:self.flashlightThreshold];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [self saveAmbientLightThreshold];
    
    return YES;
}

#pragma mark - ESTDeviceConnectableDelegate Delegate

- (void)estDeviceConnectionDidSucceed:(ESTDeviceConnectable *)device
{
    [self.activityIndicator stopAnimating];
    self.connectionStatusLabel.text = @"Connected!";
    
    [self readCurrentAmbientLightSettings];
    [self scheduleAmbientLightReadings];
}

- (void)estDevice:(ESTDeviceConnectable *)device didDisconnectWithError:(NSError *)error
{
    [self.activityIndicator stopAnimating];
    self.connectionStatusLabel.text = @"Disconnected!";
}

- (void)estDevice:(ESTDeviceConnectable *)device didFailConnectionWithError:(NSError *)error
{
    NSLog(@"Something went wrong. Beacon connection Did Fail. Error: %@", error);
    
    [self.activityIndicator stopAnimating];
    
    self.connectionStatusLabel.text = @"Connection failed";
    self.connectionStatusLabel.textColor = [UIColor redColor];
    
    UIAlertView* errorView = [[UIAlertView alloc] initWithTitle:@"Connection error"
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    
    [errorView show];
}

#pragma mark - Private

- (void)connect
{
    self.beacon.delegate = self;
    [self.beacon connect];
}

- (void)readCurrentAmbientLightSettings
{
    ESTSettingDarkToSleep *darkToSleep = [[ESTSettingDarkToSleep alloc] initWithCompletion:^(BOOL enable, NSError *error) {
        self.darkToSleepSwitch.hidden = NO;
        [self.darkToSleepSwitch setOn:enable];
    }];
    
    ESTSettingDarkToSleepThreshold *threshold = [[ESTSettingDarkToSleepThreshold alloc] initWithCompletion:^(NSNumber *threshold, NSError *error) {
        self.thresholdTextfield.text = [NSString stringWithFormat:@"%@ lx", threshold];
    }];
    
    [self.beacon readSettings:@[darkToSleep, threshold]];
}

- (void)scheduleAmbientLightReadings
{
    /**
     * Ambient light data is refreshed every two seconds.
     */
    self.ambientLightTimer = [NSTimer scheduledTimerWithTimeInterval:kRefreshRate
                                                              target:self
                                                            selector:@selector(readAmbientLight)
                                                            userInfo:nil
                                                             repeats:YES];
}

- (void)readAmbientLight
{
    ESTSettingAmbientLight *light = [[ESTSettingAmbientLight alloc] initWithCompletion:^(NSNumber *illuminance, NSError *error) {
        
        // illuminance - current ambient light
        NSLog(@"Illuminance: %@", illuminance);
        self.illuminanceLabel.text = [NSString stringWithFormat:@"%@ lx", illuminance];
        [self switchTorchModeStateBasedOnIlluminance:illuminance];
    }];
    
    [self.beacon readSetting:light];
}

- (void)switchTorchModeStateBasedOnIlluminance:(NSNumber *)illuminance
{
    AVCaptureDevice *backCamera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ( [backCamera isTorchAvailable] && [backCamera isTorchModeSupported:AVCaptureTorchModeOn]  )
    {
        BOOL success = [backCamera lockForConfiguration:nil];
        if ( success )
        {
            BOOL onOff = [illuminance integerValue] < [self.flashlightThreshold integerValue];
            [backCamera setTorchMode:(AVCaptureTorchMode)onOff];
            [backCamera unlockForConfiguration];
        }
    }
}

#pragma mark - Saving Methods

/**
 * As you know next gen beacon can transmit different packets.
 * Since 4.0.0 they are: Estimote service and iBeacon.
 * Estimote service packet is always transmitted no matter if you turned on
 * flip to sleep or dark to sleep.
 * Dark to sleep works similar to flip to sleep but instead of depending on 
 * accelerometer data beacon checks current ambient light conditions.
 * If current lux reading are below certain threshold (that's where you setup 
 * DarkToSleepThreshold) after ~20 seconds beacon stops advertising iBeacon packet.
 */

- (void)saveAmbientLightThreshold
{
    ESTSettingDarkToSleepThreshold *threshold;
    
    NSNumber *toSave = @([self.thresholdTextfield.text integerValue]);
    
    threshold = [[ESTSettingDarkToSleepThreshold alloc] initWithValue:toSave completion:^(NSNumber *threshold, NSError *error) {

        NSLog(@"Threshold saved: %@ error: %@", threshold, error);
    }];
    
    [self.beacon writeSetting:threshold];
}

- (IBAction)switchDarkToSleep:(UISwitch *)sender
{
    ESTSettingDarkToSleep *darkToSleep = [[ESTSettingDarkToSleep alloc] initWithValue:sender.isOn completion:^(BOOL enable, NSError *error) {
        
        NSLog(@"Dark to sleep saved: %@ error: %@", @(enable), error);
    }]; 
    
    [self.beacon writeSetting:darkToSleep];
}

@end
