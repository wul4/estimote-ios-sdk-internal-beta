//
//  ESTVirtualBeaconDemoVC.m
//  Examples
//
//  Created by Marcin Klimek on 23/04/15.
//  Copyright (c) 2015 com.estimote. All rights reserved.
//

#import "ESTVirtualBeaconDemoVC.h"
#import <EstimoteSDK/EstimoteSDK.h>

@interface ESTVirtualBeaconDemoVC () <ESTBeaconManagerDelegate>

@property (nonatomic, strong) ESTBeaconManager *beaconManager;

@end

@implementation ESTVirtualBeaconDemoVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Virtual Beacon";
    
    self.beaconManager = [ESTBeaconManager new];
    self.beaconManager.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.beaconManager startAdvertisingWithProximityUUID:ESTIMOTE_PROXIMITY_UUID
                                                    major:1234
                                                    minor:1234
                                               identifier:@"VirtualBeacon"];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.beaconManager stopAdvertising];
}

#pragma mark - ESTBeaconManager delegate handling

- (void)beaconManagerDidStartAdvertising:(id)manager error:(NSError *)error
{
    if (error)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Advertising error!"
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        [alertView show];
    }
}

@end
