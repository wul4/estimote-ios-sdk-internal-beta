//
//  Copyright (c) 2015 Estimote, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EstimoteSDK/EstimoteSDK.h>


@interface ESTAmbientLightDemoVC : UIViewController

- (instancetype)initWithDeviceTypeUtility:(ESTDeviceTypeUtility *)device;

@end
