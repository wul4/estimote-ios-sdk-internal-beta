//
//   ______     _   _                 _          _____ _____  _  __
//  |  ____|   | | (_)               | |        / ____|  __ \| |/ /
//  | |__   ___| |_ _ _ __ ___   ___ | |_ ___  | (___ | |  | | ' /
//  |  __| / __| __| | '_ ` _ \ / _ \| __/ _ \  \___ \| |  | |  <
//  | |____\__ \ |_| | | | | | | (_) | ||  __/  ____) | |__| | . \
//  |______|___/\__|_|_| |_| |_|\___/ \__\___| |_____/|_____/|_|\_\
//
//
//  Copyright (c) 2015 Estimote. All rights reserved.

#import <Foundation/Foundation.h>
#import "ESTSettingProtocol.h"

typedef void(^ESTSettingMinorCompletionBlock)(NSNumber *minor, NSError *error);


@interface ESTSettingMinor : NSObject <ESTSettingProtocol, NSCopying>

+ (instancetype)settingWithCompletion:(ESTSettingMinorCompletionBlock)completion;

+ (instancetype)settingWithValue:(NSNumber *)minor
                      completion:(ESTSettingMinorCompletionBlock)completion;

- (instancetype)initWithCompletion:(ESTSettingMinorCompletionBlock)completion;

- (instancetype)initWithValue:(NSNumber *)minor
                   completion:(ESTSettingMinorCompletionBlock)completion;
@end
