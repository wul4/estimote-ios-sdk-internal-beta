//
//   ______     _   _                 _          _____ _____  _  __
//  |  ____|   | | (_)               | |        / ____|  __ \| |/ /
//  | |__   ___| |_ _ _ __ ___   ___ | |_ ___  | (___ | |  | | ' /
//  |  __| / __| __| | '_ ` _ \ / _ \| __/ _ \  \___ \| |  | |  <
//  | |____\__ \ |_| | | | | | | (_) | ||  __/  ____) | |__| | . \
//  |______|___/\__|_|_| |_| |_|\___/ \__\___| |_____/|_____/|_|\_\
//
//
//  Copyright (c) 2015 Estimote. All rights reserved.

#import <Foundation/Foundation.h>
#import "ESTSettingProtocol.h"

typedef void(^ESTSettingMajorCompletionBlock)(NSNumber *major, NSError *error);

@interface ESTSettingMajor : NSObject <ESTSettingProtocol, NSCopying>

+ (instancetype)settingWithCompletion:(ESTSettingMajorCompletionBlock)completion;

+ (instancetype)settingWithValue:(NSNumber *)major
                      completion:(ESTSettingMajorCompletionBlock)completion;

- (instancetype)initWithCompletion:(ESTSettingMajorCompletionBlock)completion;

- (instancetype)initWithValue:(NSNumber *)major
                    completion:(ESTSettingMajorCompletionBlock)completion;
@end
