//
//  ESTSettingHardwareVersion.h
//  EstimoteSDK
//
//  Created by Marcin Klimek on 23/07/15.
//  Copyright (c) 2015 Estimote. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESTSettingProtocol.h"

typedef void (^ESTSettingHardwareVersionCompletionBlock)(NSString *version, NSError *error);

@interface ESTSettingHardwareVersion : NSObject <ESTSettingProtocol, NSCopying>

- (instancetype)initWithCompletion:(ESTSettingHardwareVersionCompletionBlock)completion;

@end
