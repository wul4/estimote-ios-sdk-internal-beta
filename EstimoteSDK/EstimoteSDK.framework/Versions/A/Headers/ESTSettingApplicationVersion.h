//
//  ESTSettingFirmwareVersion.h
//  EstimoteSDK
//
//  Created by Marcin Klimek on 23/07/15.
//  Copyright (c) 2015 Estimote. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESTSettingProtocol.h"

typedef void (^ESTSettingApplicationVersionCompletionBlock)(NSString *version, NSError *error);

@interface ESTSettingApplicationVersion : NSObject <ESTSettingProtocol, NSCopying>

- (instancetype)initWithCompletion:(ESTSettingApplicationVersionCompletionBlock)completion;

@end
