//
//   ______     _   _                 _          _____ _____  _  __
//  |  ____|   | | (_)               | |        / ____|  __ \| |/ /
//  | |__   ___| |_ _ _ __ ___   ___ | |_ ___  | (___ | |  | | ' /
//  |  __| / __| __| | '_ ` _ \ / _ \| __/ _ \  \___ \| |  | |  <
//  | |____\__ \ |_| | | | | | | (_) | ||  __/  ____) | |__| | . \
//  |______|___/\__|_|_| |_| |_|\___/ \__\___| |_____/|_____/|_|\_\
//
//  Copyright (c) 2015 Estimote. All rights reserved.

#import <Foundation/Foundation.h>
#import "ESTDeviceConnectable.h"
#import "ESTFirmwareInfoV4VO.h"

@class ESTDeviceTypeUtilitySettings;
@class CBPeripheral;
@class ESTPeripheralTypeUtility;

#define ESTDeviceTypeUtilityErrorDomain @"ESTDeviceTypeUtilityErrorDomain"

typedef NS_ENUM(NSInteger, ESTDeviceTypeUtilityError)
{
    ESTDeviceTypeUtilityErrorDeviceNotConnected,
    ESTDeviceTypeUtilityErrorSettingWriteValueMissing,
    ESTDeviceTypeUtilityErrorSettingCloudSaveFailed,
    ESTDeviceTypeUtilityErrorSettingGroupWriteFailed,
    ESTDeviceTypeUtilityErrorSettingNotSupported,
    ESTDeviceTypeUtilityErrorConnectionCloudConfirmationFailed,
    ESTDeviceTypeUtilityErrorFirmwareUpdateNoUpdate
};


@interface ESTDeviceTypeUtility : ESTDeviceConnectable

- (instancetype)initWithPublicID:(NSString *)publicID
            peripheralIdentifier:(NSUUID *)peripheralID
                            RSSI:(NSNumber *)rssi
                      appVersion:(NSString *)appVersion
               bootloaderVersion:(NSString *)bootloaderVersion;

@property (nonatomic, readonly) NSString *publicID;

@property (nonatomic, readonly) NSString *applicationVersion;
@property (nonatomic, readonly) NSString *bootloaderVersion;

@property (nonatomic, readonly) CBPeripheral *peripheral;

- (ESTDeviceTypeUtilitySettings *)getCurrentSettings;

- (void)checkFirmwareUpdateWithCompletion:(ESTObjectCompletionBlock)completion;

- (void)updateFirmwareWithProgress:(ESTProgressBlock)progress completion:(ESTCompletionBlock)completion;

@end
