//
//   ______     _   _                 _          _____ _____  _  __
//  |  ____|   | | (_)               | |        / ____|  __ \| |/ /
//  | |__   ___| |_ _ _ __ ___   ___ | |_ ___  | (___ | |  | | ' /
//  |  __| / __| __| | '_ ` _ \ / _ \| __/ _ \  \___ \| |  | |  <
//  | |____\__ \ |_| | | | | | | (_) | ||  __/  ____) | |__| | . \
//  |______|___/\__|_|_| |_| |_|\___/ \__\___| |_____/|_____/|_|\_\
//
//
//  Copyright (c) 2015 Estimote. All rights reserved.

#import <Foundation/Foundation.h>
#import "ESTSettingProtocol.h"

typedef void(^ESTSettingDarkToSleepThresholdCompletionBlock)(NSNumber *threshold, NSError *error);

@interface ESTSettingDarkToSleepThreshold : NSObject <ESTSettingProtocol, NSCopying>

- (instancetype)initWithCompletion:(ESTSettingDarkToSleepThresholdCompletionBlock)completion;

/**
* Threshold is a value of lux in range from 0 to 180000
* Default threshold is 2 lux. 
*/
- (instancetype)initWithValue:(NSNumber *)threshold
                   completion:(ESTSettingDarkToSleepThresholdCompletionBlock)completion;

@end
