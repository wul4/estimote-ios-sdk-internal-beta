//
//   ______     _   _                 _          _____ _____  _  __
//  |  ____|   | | (_)               | |        / ____|  __ \| |/ /
//  | |__   ___| |_ _ _ __ ___   ___ | |_ ___  | (___ | |  | | ' /
//  |  __| / __| __| | '_ ` _ \ / _ \| __/ _ \  \___ \| |  | |  <
//  | |____\__ \ |_| | | | | | | (_) | ||  __/  ____) | |__| | . \
//  |______|___/\__|_|_| |_| |_|\___/ \__\___| |_____/|_____/|_|\_\
//
//
//  Copyright (c) 2015 Estimote. All rights reserved.

#import <Foundation/Foundation.h>
#import "ESTSettingProtocol.h"

typedef void(^ESTSettingProximityUUIDCompletionBlock)(NSUUID *proximityUUID, NSError *error);


@interface ESTSettingProximityUUID : NSObject <ESTSettingProtocol, NSCopying>

+ (instancetype)settingWithCompletion:(ESTSettingProximityUUIDCompletionBlock)completion;

+ (instancetype)settingWithValue:(NSUUID *)proximityUUID
                      completion:(ESTSettingProximityUUIDCompletionBlock)completion;

- (instancetype)initWithCompletion:(ESTSettingProximityUUIDCompletionBlock)completion;

- (instancetype)initWithValue:(NSUUID *)proximityUUID
                   completion:(ESTSettingProximityUUIDCompletionBlock)completion;

@end
