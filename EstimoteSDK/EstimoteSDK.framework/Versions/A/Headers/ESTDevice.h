//  Copyright (c) 2015 Estimote. All rights reserved.

#import <Foundation/Foundation.h>

@interface ESTDevice : NSObject

@property (nonatomic, strong, readonly) NSString *macAddress __attribute ((deprecated(("Starting from SDK 4.0.0-beta1 use identifier instead of macAddress."))));
@property (nonatomic, strong, readonly) NSString *identifier;

@property (nonatomic, strong, readonly) NSUUID *peripheralIdentifier;
@property (nonatomic, assign, readonly) NSInteger rssi;

@property (nonatomic, strong, readonly) NSDate *discoveryDate;
@property (nonatomic, strong, readonly) NSData *advertisementData;

@end
