//
//   ______     _   _                 _          _____ _____  _  __
//  |  ____|   | | (_)               | |        / ____|  __ \| |/ /
//  | |__   ___| |_ _ _ __ ___   ___ | |_ ___  | (___ | |  | | ' /
//  |  __| / __| __| | '_ ` _ \ / _ \| __/ _ \  \___ \| |  | |  <
//  | |____\__ \ |_| | | | | | | (_) | ||  __/  ____) | |__| | . \
//  |______|___/\__|_|_| |_| |_|\___/ \__\___| |_____/|_____/|_|\_\
//
//
//  Copyright (c) 2015 Estimote. All rights reserved.


#import <Foundation/Foundation.h>
#import "ESTSettingProtocol.h"

typedef void (^ESTSettingBatteryLevelCompletionBlock) (NSNumber *batteryVoltage, NSError *error);

/**
 * Battery voltage parameter is measured in millivolts (mV).
 */
@interface ESTSettingBatteryLevel : NSObject <ESTSettingProtocol, NSCopying>

- (instancetype)initWithCompletion:(ESTSettingBatteryLevelCompletionBlock)completion;

@end
