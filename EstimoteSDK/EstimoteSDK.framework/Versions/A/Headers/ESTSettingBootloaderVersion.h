//
//  ESTSettingBootloaderVersion.h
//  EstimoteSDK
//
//  Created by Marcin Klimek on 23/07/15.
//  Copyright (c) 2015 Estimote. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESTSettingProtocol.h"

typedef void (^ESTSettingBootloaderVersionCompletionBlock)(NSString *version, NSError *error);

@interface ESTSettingBootloaderVersion : NSObject <ESTSettingProtocol, NSCopying>

- (instancetype)initWithCompletion:(ESTSettingBootloaderVersionCompletionBlock)completion;

@end
