//  Copyright (c) 2015 Estimote. All rights reserved.

#import <Foundation/Foundation.h>
#import "ESTDeviceTypeUtilitySettings.h"


@protocol ESTSettingProtocol <NSObject>

@required

/**
 *  ID of register should be requested to read or write value for setting.
 *
 *  @return NSInteger value representing register ID
 */
- (NSInteger)registerID;

/**
 *  Method invoked when read/write operation finished with success.
 *
 *  @param result result of read/write operation
 */
- (void)fireSuccessBlockWithData:(NSData *)result;

/**
 *  Method invoked when read/write operation failed.
 *
 *  @param error NSError containing failure information
 */
- (void)fireFailureBlockWithError:(NSError *)error;

/**
 *  Method returns value of setting.
 *
 *  @return Value of setting. Type of value depends on the setting.
 */
- (id)getValue;

/**
 *  Method returns value converted to NSDate object that can be used
 *  to save value to the device.
 *
 *  @return NSDate representing setting value
 */
- (NSData *)getValueData;

/**
 *  Method updates setting value based on NSData object delivered from the device.
 *  Parsing is done during the operation.
 *
 *  @param data NSData delivered from the device.
 */
- (void)updateValueWithData:(NSData *)data;

/**
 *  Methods indicates if particular settings should be tracked by device settings manager
 *  and saved in the cloud when value change.
 *
 *  @return BOOL value indicating tracking
 */
- (BOOL)shouldTrackSetting;

/**
 *  Decorator method that should update particular field in settings object
 *  with proper value it is carrying.
 *
 *  @param settings Settings object that should be decorated
 */
- (void)updateValueInSettings:(ESTDeviceTypeUtilitySettings *)settings;

/**
 *  Allows to validate if particular settings is available for firmware
 *  in version running on the device.
 *
 *  @param firmware Version of firmware running on the device
 *
 *  @return BOOL value indicating availability
 */
- (BOOL)isAvailableForFirmwareVersion:(NSString *)firmwareVersion;

@optional

/**
* Implement this method to check if value passed
* to your setting is correct.
* Return nil if it is, otherwise return error with description.
* You should use ESTErrorCodeInvalidValue as a error code.
*/
- (NSError *)validateValue;

@end
